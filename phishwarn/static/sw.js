const cacheName = 'site-static-v1';
const contentToCache = [
  '/',
  '/css/',
  '/de/',
  '/index.html',
  '/tutorials/how-to-prevent-being-phished/',
  '/tutorials/how-to-recognize-phishing/',
  '/de/index.html',
  '/de/tutorials/how-to-prevent-being-phished/',
  '/de/tutorials/how-to-recognize-phishing/',
  '/css/phishwarn.css',
  '/css/dark.css',
  '/css/darkmode.css',
  '/img/phishwarn-logo.png',
  '/img/berkeley.png',
  '/img/ncsc.png',
  '/img/bsi.png',
  '/img/ftc-preview.png',
  '/img/mobilsicher-preview.png',
  '/img/kuketz-preview.png',
  '/img/phishing-vr.png'
];

// install event
self.addEventListener('install', (e) => {
  console.log('[Service Worker] Install');
  e.waitUntil((async () => {
    const cache = await caches.open(cacheName);
    console.log('[Service Worker] Caching all: app shell and content');
    await cache.addAll(contentToCache);
  })());
});

// activate event
self.addEventListener('activate', (e) => {
  e.waitUntil(caches.keys().then((keyList) => {
    return Promise.all(keyList.map((key) => {
      if (key === cacheName) { return; }
      return caches.delete(key);
    }))
  }));
});

// fetch event
self.addEventListener('fetch', (e) => {
  e.respondWith((async () => {
    const r = await caches.match(e.request);
    console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
    if (r) { return r; }
    const response = await fetch(e.request);
    const cache = await caches.open(cacheName);
    console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
    cache.put(e.request, response.clone());
    return response;
  })());
});