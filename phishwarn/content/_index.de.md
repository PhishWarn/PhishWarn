---
title: Du hättest gerade auf einen Phishing-Link geklickt!
type: article
layout: index

links:
  - berkeley.de
  - ncsc.de
  - vbz.de
---

{{< contentHeadline >}}Du hättest gerade auf einen Phishing-Link geklickt!{{< /contentHeadline >}}

{{< articleRed >}}
Irgendwo hast du gerade auf einen Link geklickt, der dir legitim erschien, in Wirklichkeit aber gefälscht war. Der Ort, von dem du hierher gekommen bist, kann eine Scam-E-Mail, eine betrügerische Chat-Nachricht oder eine andere Website sein.
{{< /articleRed >}}

{{< article >}}
{{< important >}}Wie dem auch sei, der Ersteller des Links wollte dir nicht tatsächlich Schaden zufügen.{{< /important >}} Stattdessen hat er diese Website verlinkt, um dir deine eigene Naivität vor Augen zu halten und zu demonstrieren, wie einfach es ist, jemanden dazu zu bringen, einen gefälschten Link anzuklicken.
{{< /article >}}

{{< article >}}
{{< articleHeadline >}}Was ist Phishing?{{< /articleHeadline >}}

Phishing bedeutet, dass ein Betrüger eine Nachricht von einem legitimen Absender fälscht, um das Opfer dazu zu bringen, schädliche Anweisungen auszuführen, beispielsweise auf einen vermeintlich legitimen Link zu klicken, sich in einen Online-Account einzuloggen oder Geld zu überweisen.

Beispielsweise senden einige Betrüger gefälschte E-Mails von PayPal, in denen steht, dass dein Account „gehackt wurde und du dich deshalb sofort bei PayPayl einloggen musst, um ein Passwort zu ändern“.

Andere senden E-Mails, die scheinbar von deiner Bank stammen und in denen steht, dass deine Kreditkarte ablaufen würde, falls du dich nicht in deinen Account einloggst.

Manche Opfer erhalten auch „Amazon“-Nachrichten, in denen steht, dass ihr Account gesperrt würde, sollten sie innerhalb von 48 Stunden keine Sicherheits-Überprüfung machen.
{{< /article >}}

{{< figure "phishing-explanation/amazon-phishing-mail.png" "Amazon-Phishing-E-Mail" >}}
Eine angeblich von Amazon stammende E-Mail, in welcher das Opfer auf einen Button klicken soll. Gut zu wissen: Amazon {{< link "https://www.amazon.de/gp/help/customer/display.html?nodeId=G4YFYCCNUSENA23B" >}}bittet{{< /link >}} seine Nutzer, „Amazon“-Phishing-Mails an {{< link "mailto:stop-spoofing@amazon.com" >}}stop-spoofing@amazon.com{{< /link >}} zu melden.
{{< /figure >}}

{{< article >}}
{{< articleHeadline >}}Warum funktioniert Phishing?{{< /articleHeadline >}}

Meistens versuchen Phishing-Nachrichten das Opfer zu erschrecken und panisch werden zu lassen, sodass es nicht mehr rational denkt. Wenn sie erfolgreich sind, denkt das Opfer nicht mehr kritisch über den Inhalt der Nachricht nach und befolgt einfach sämtliche Anweisungen.
{{< /article >}}

{{< teaser "preventPhishing" "de" >}}

{{< teaser "recognizePhishing" "de" >}}
