---
title: You would have just clicked on a phishing link!
type: article
layout: index

links:
  - berkeley.en
  - ncsc.en
  - vbz.en
---

{{< contentHeadline>}}You would have just clicked on a phishing link!{{< /contentHeadline >}}
{{< articleRed >}}

Somewhere, you have clicked on a link that seemed legit to you but which was in fact not legit. The place you came from might have been a scam e-mail, a fraudulent chat message or another website.
{{< /articleRed >}}

{{< article >}}
{{< important >}}However, the creator of the link didn’t intend to actually do any harm to you.{{< /important >}} Instead, he/she linked to this website in order to demonstrate your own naivety to you and to show how easy it is to trick someone into clicking on a non-legit link.
{{< /article >}}

{{< article >}}
{{< articleHeadline id "content">}}What’s phishing?{{< /articleHeadline >}}

Phishing means that a fraudster fakes a message from a legit source to make the victim follow malicious instructions, such as clicking on a seemingly legit link, logging into some online account, sending money etc.

For example, some scammers send fake PayPal e-mails saying that your account “was hacked and you must therefore immediately log into PayPal to change your password”.

Others send e-mails seemingly from your bank saying that your credit card will expire if you don’t log in to your account.

Some victims also receive “Amazon” messages saying that your account will be blocked if you don’t do a “security validation” within 48 hours.

{{< /article >}}

{{< figure "phishing-explanation/netflix-phishing.jpg" "Netflix phishing e-mail" >}}
An e-mail seemingly from Netflix urging the victim to click on a button. Good to know: Netflix {{< link "https://help.netflix.com/en/node/65674" >}}ask{{< /link >}} their users to redirect “Netflix” phishing e-mails to {{< link "mailto:phishing@netflix.com" >}}phishing@netflix.com{{< /link >}}.
{{< /figure >}}

{{< article >}}
{{< articleHeadline >}}Why does phishing work?{{< /articleHeadline >}}
Mostly, phishing messages try to scare the victim and to make him/her panick so that he/she doesn’t think rationally anymore. If they’re successful, the victim doesn’t critically think about the message’s content and simply follows any instructions given.
{{< /article >}}

{{< teaser "preventPhishing" "en" >}}

{{< teaser "recognizePhishing" "en" >}}
