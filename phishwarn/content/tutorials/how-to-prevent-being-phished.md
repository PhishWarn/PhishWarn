---
title: How to prevent being phished?
description: How to prevent becoming a victim of phishing in the first place?
layout: tutorials

links:
  - ftc.en
  - ncsc.en
  - vbz.de
---
{{< contentHeadline >}}How to prevent being phished?{{< /contentHeadline >}}
{{< article >}}
Obviously, it’s impossible to prevent phishing attempts in general. However, here are some online security measures that can help you avoid scam.

{{< subHeadline "top" >}}Use ad blockers{{< /subHeadline >}}

Ad blockers are applications preventing that online advertisements are loaded into websites or apps. However, many of them also support filter lists with malicious websites to prevent you from accidentally visiting them. Really great ad blockers are {{< link "https://github.com/gorhill/uBlock#ublock-origin">}}uBlock Origin{{< /link >}} (browser addon) and {{< link "https://blokada.org">}}Blokada{{< /link >}} (Android/iOS app).

{{< subHeadline >}}Ask the sender separately whether he actually sent the message{{< /subHeadline >}}

This “trick” is so easy but yet so effective: Just ask the alleged sender via a different means of communication whether he actually sent the message. Received a weird SMS from your friend? Send him an e-mail. Got a suspicious e-mail from your bank? Find their phone number on their actual website and call them. Threatened to be arrested by the police? Go to the next police station and ask what’s wrong. (Conveniently, you can also report Presumption of Authority there.)

{{< subHeadline >}}Keep your e-mail address and phone number private{{< /subHeadline >}}

Fraudsters can only send you phishing texts if they know your contact details. Keep them private and only use
secondary e-mail addresses or phone numbers for online registrations etc.				

{{< details >}}
Mostly, scammers get your e-mail address or telephone number via newsletter lists, online shop accounts or internet raffles. That’s why you should give information only to people you personally know.
<br><br>
Use designated secondary e-mail addresses for online registration so you know before that there might be spam and scam in the respective inbox. Try using {{< link "https://relay.firefox.com">}}Firefox Relay{{< /link >}} to keep your e-mail address private.
<br><br>
Also, consider buying a cheap secondary SIM card with a seperate number in order to, for example, register with messengers like Signal that require a mobile number.
{{< /details >}}

{{< subHeadline >}}Keep your software up-to-date{{< /subHeadline >}}

Hackers and scammers often make use of outdated software: For example, they misuse vulnerabilities to access foreign computers or they take advantage of the fact that blacklists of malicious websites are not updated and maintained. That’s why you should activate automatic updates wherever possible in order to receive security and bug fixes and the latest filter lists.

{{< subHeadline >}}Disable Punycode{{< /subHeadline >}}

Many fraudsters register their own, legitimately-looking domains where they replace e.g. the Latin a by the
the confusingly similar looking Cyrillic а. Disable the display of such special characters in your browser
(c.f. this {{< link "https://thehackernews.com/2017/04/unicode-Punycode-phishing-attack.html">}}Hacker News article{{< /link >}}) or use the browser addon {{< link "https://addons.mozilla.org/firefox/addon/punycode-domain-detection/">}}PunyCode Domain Detection{{< /link >}} in order to get notified when visiting domains with special characters.

{{< details >}}
Especially Western users know the internet almost only with Latin characters and have only heard of Cyrillic or Arabic script in school. In order to display e.g. Ukranian or Taiwanese URLs correctly too, modern browsers use the technology "Punycode" which also lets scammers disguise their malicious domains with special characters looking similar to Latin letters.
<br><br>
To avoid falling for actual phishing in the future, you should either completely disable Punycode in your browser or use the addon {{< link "https://addons.mozilla.org/firefox/addon/punycode-domain-detection/" >}}PunyCode Domain Detection{{< /link >}} which notifies you of URLs with Punycode.
<br><br>
In Firefox, you can disable Punycode by typing about:config into the address bar (also works on mobile phones) and looking for the setting “network.IDN_show_punycode” which you set to “true”.
{{< /details >}}

{{< figure "tutorials/apple.com.png" "Screenshot of a website whose address looks like apple.com">}}
This Punycode {{< link "https://www.аррӏе.com/" >}}demo page{{< /link >}} seems to have the address {{< important >}}apple.com{{< /important >}}, but in reality it consists completely of Cyrillic letters: аррӏе.
{{< /figure >}}

{{< figure "tutorials/xn--80ak6aa92e.com.png" "Screenshot of the same page, this time clearly recognizable as not apple.com" >}}
When disabling Punycode in the browser, the address is displayed as {{< important >}}xn--80ak6aa92e.com{{< /important >}} – definitly not apple.com.
{{< /figure >}}

{{< figure "tutorials/apple.com addon.png" "“possible phishing attempt: the address bar shows apple.com but the real domain name is xn--80ak6aa92e.com”">}}
The anti-Punycode addon warns about a possible phishing attempt.
{{< /figure >}}

{{< subHeadline >}}Don’t reveal too much about yourself on social media{{< /subHeadline >}}

Scammers use open-source investigation techniques to learn more about potential victims, for example by checking their social media profiles. That’s why you should always think about what you post online and whether it could be misused by fraudsters. Check whether you can limit the visibility of your posts to friends and family. Try locking your entire account so that only people you know can see it.

Read more about the topic at {{< link "https://www.ncsc.gov.uk/guidance/social-media-how-to-use-it-safely">}}NCSC{{< /link >}}.

{{< subHeadline >}}Use multi-factor authentification{{< /subHeadline >}}

Surely, you have already heard the term “2FA”. It stands for “two-factor authentification” and means that you need more than just a password to log into online accounts, e.g. an “OTP” code, your fingerprint or a safety USB stick. Activate 2FA wherever possible to prevent others from accessing your accounts just by guessing your password.

EFF tells you {{< link "https://ssd.eff.org/en/module/how-enable-two-factor-authentication">}}how to do it{{< /link >}}.

{{< subHeadline >}}Use a password manager{{< /subHeadline >}}

Don’t use the same password for every website, don’t use easy-to-guess passwords, don’t write them down. But how to remember dozens of complicated text strings? Well, you don’t. Instead, use a password manager – a digital vault for all your different passwords.

PhishWarn recommends
{{< link "https://keepassxc.org">}}KeePassXC{{< /link >}}.

{{< subHeadline >}}Inform yourself regularly in the relevant media{{< /subHeadline >}}

In order to stay up to date on what new tricks the scammers have come up with and how you can protect yourself, you should regularly inform yourself about the topic in relevant media.

{{< details >}}
For example, PhishWarn recommends the technology magazines
<br>
<br>- {{< link "https://www.wired.co.uk/topic/security">}}Wired{{< /link >}},
<br>- {{< link "https://www.zdnet.com">}}ZDNet{{< /link >}},
<br>- {{< link "https://www.vice.com/en/topic/phishing">}}Vice{{< /link >}} and
<br>- {{< link "https://arstechnica.com">}}Ars Technica{{< /link >}},
<br><br>
but also smaller blogs like {{< link "https://grahamcluley.com">}}Graham Clueley’s blog{{< /link >}} and his {{< link "https://www.smashingsecurity.com">}}Smashing Security podcast{{< /link >}} can often be very enlightening. Government agencies also have their own information services, e.g.:
<br>
<br>- the UK’s {{< link "https://www.ncsc.gov.uk">}}National Cyber Security Centre{{< /link >}} or
<br>- the online security section of the good old {{< link "https://www.fbi.gov/scams-and-safety">}}FBI{{< /link >}}.
	{{< /details >}}
{{< /article >}}
{{< teaser "recognizePhishing" "en" >}}