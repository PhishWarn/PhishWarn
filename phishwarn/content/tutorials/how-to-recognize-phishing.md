---
title: How to recognize phishing?
description: Learn more about how to recognize phishing attempts.
layout: tutorials

links:
  - ftc.en
  - ncsc.en
  - vbz.de
---
{{< contentHeadline >}}How to recognize phishing?{{< /contentHeadline >}}
{{< article >}}
Recognizing phishing can be quite difficult. Here are some indicators that may tell you a message is suspicious.

{{< subHeadline "top" >}}Non-professional address{{< /subHeadline >}}

Professional companies use their own domains for their e-mail addresses instead of using free mail services, e.g.: support@twitter.com instead of twitter@yahoo.com.

{{< details >}}
Professional companies almost always have a website with a customized domain name, such as apple.com for the smartphone vendor Apple or nytimes.com for the New York Times. Whenever you receive an e-mail from a company you should search the internet for their website domain and check whether it corresponds with the e-mail domain.
<br><br>
Let’s say, you received an e-mail seemingly from Twitter. If you don’t know already, you quickly find out that their domain is called {{< important >}}twitter.com{{< /important >}}. Now, what’s the sender’s e-mail address? Let’s say it’s {{< important >}}support@twitter.com{{< /important >}}. The domain is the part behind the @ symbol, so in this case it’s {{< important >}}twitter.com{{< /important >}}. That definitly seems legit.
<br><br>
But what if the sender is {{< important >}}twitter@mail.com{{< /important >}}? There’s the word “Twitter” in it too. The difference is: It’s just a random user name which is not unique. The domain is {{< important >}}mail.com{{< /important >}}, a popular e-mail provider for private users. A company like Twitter would never use such an unprofessional mail service when they could use their shiny twitter.com domain.
{{< /details >}}

{{< subHeadline >}}Fake support of free e-mail service{{< /subHeadline >}}

Some scammers don’t try imitating a company by using a free service like with the twitter@yahoo.com example above. Instead, they try to imitate the free e-mail service {{< important >}}itself{{< /important >}}, e.g.: {{< important >}}friendly-user-support@yahoo.com{{< /important >}}. This seems to be Yahoo’s actual customer support but it’s actually just a random user account that happens to have a “weird” user name. That’s why you should always google
whether the sender’s e-mail address actually belongs to your e-mail service’s support.

{{< subHeadline >}}Wrong account address{{< /subHeadline >}}

A fraudster usually doesn’t know which e-mail address belongs to which online account (or vice versa) so you may receive a phishing e-mail seemingly from Amazon to an e-mail address of you which doesn’t belong to your actual Amazon account. And as Amazon would never ever send any e-mails to addresses that don’t belong to the respective user account you can be quite sure that e-mails to the wrong address are actually phishing attempts.

{{< subHeadline >}}Messages from services you don’t use{{< /subHeadline >}}

Scammers just imitate any popular company/service out there because then the chances are quite high that the randomly chosen victim actually uses the respective service. But let’s say, you receive an e-mail from TikTok but you don’t even use TikTok – then it’s obviously a fake.

{{< subHeadline >}}Sub-domain instead of domain{{< /subHeadline >}}

Some scammers buy own domains for their e-mail addresses and make them look like legit ones, e.g.: {{< important >}}support@amazon.com.beispiel.de{{< /important >}}. It looks like the domain was amazon.com but it’s beispiel.de instead.

{{< details >}}
Many scammers don’t do something as simple as using twitter@mail.com anymore. Instead, they buy own domains and disguise them in a way so that they look like a legit address. A popular technique is placing a sub-domain in front of the actual domain.
<br><br>
Every web address consists of several parts. Basically, it’s this scheme:
<br><br>
{{< xScroll >}}protocol://subdomain.domain.TopLevelDomain/path/to/directory/filename.filetype{{< /xScroll >}}

Example: https://pixelcode.codeberg.page/phishwarn/index.html
<br><br>
For e-mails, only this part is relevant:
{{< important >}}subdomain.domain.TLD{{< /important >}}
<br><br>
The important thing about this is that every domain.tld is unique. There’s only one ebay.com and only one freedom.press.
<br><br>
On the other hand, {{< important >}}sub-domains are NOT unique{{< /important >}}. Every owner of a domain can set any sub-domain he likes and as many sub-domains he likes. That’s why an e-mail domain like this one could be possible: {{< important >}}amazon.com.beispiel.de{{< /important >}}.
<br><br>
Note that the actual domain is NOT amazon.com but {{< important >}}beispiel.de{{< /important >}} instead. However, many users wouldn’t know that amazon.com isn’t the domain and think that the sender actually is Amazon when instead,
amazon.com is just two sub-domains: amazon and com.
<br><br>
The real domain is always the last string before the top-level domain. The TLD is something like .com, .net, .gov.uk, .de, .fr etc. If you aren’t sure about this domain stuff, just remember that many dots within an e-mail domain is suspicious = many sub-domains.
{{< /details >}}

{{< subHeadline >}}High pressure on you{{< /subHeadline >}}

Scammers put high pressure on you, using urgency or severe consequences and pretending to be important authorities.

{{< details >}}
As explained above, scammers try to make you panick by putting high pressure on you. Here’s how they do it:
<br><br>
{{< important >}}1. Urgency:{{< /important >}} If you’re told to do a specific action within e.g. 24 or 48 hours, you should probably wait a minute. Ask yourself: Does the issue really seem that urgent? Would the real sender send such an important message to you via e-mail/SMS etc.? Why wouldn’t they send you a letter to ensure you receive their message?
<br><br>
{{< important >}}2. Severe consequences:{{< /important >}} If you’re threatened with severe consequences like an account ban, or being sued, arrested or fired, it’s the same situation: Would the real sender threaten you like that – especially via e-mail/SMS etc.?
<br><br>
{{< important >}}3. Important authority:{{< /important >}} Many fraudsters claim to be someone like your doctor, the police, the government in general or any other important authority. Stop for a moment and think about whether you’ve ever received a message from the respective sender via the means of communication used. If not: Are you sure that __the police__ would send you an e-mail? Do they even know your address? Why would they choose such an insecure means of communication when they could’ve sent a letter?
<br><br>
At least in the Western world, police would never ever send e-mails or text messages but letters instead. In urgent cases, they would send police officers to talk to you in person.
{{< /details >}}

{{< subHeadline >}}Vague form of address{{< /subHeadline >}}

Many fraudsters use vague forms of address instead of your actual name because they simply don’t know it. For example, a bank would never use something like “Valued customer...” or “Dear Sir or Madam...” in a message with customer-specific contents. So if the sender doesn’t mention your name but should know it if he was actually legit, then you should be alert.

{{< subHeadline >}}Poor grammar and spelling{{< /subHeadline >}}

Some phishing messages contain many and severe grammar and spelling mistakes – unlike legit messages from professional companies
that ensure they don’t damage their reputation.

{{< details >}}
Legit senders, especially huge companies would most likely pay close attention to anything that could damage their reputation, including poor grammar and spelling. Here are some reasons why phishing texts may be linguistically incorrect:
<br><br>
{{< important >}}1. Bad online translation:{{< /important >}} Many scammers use online translation services of low quality to distribute their phishing messages in different languages. Google Translate’s poor quality has even become an internet meme.
<br><br>
{{< important >}}2. Not a native speaker:{{< /important >}} Many scammers simply come from foreign countries where the victim’s mother tongue isn’t a lingua franca. Many of them also come from poor regions of the world where there’s no proper education in general.
<br><br>
{{< important >}}3. Efficiency:{{< /important >}} It might be more efficient to send out thousands of phishing mails without proper grammar than manufacturing a few high-quality mails.
<br><br>
{{< important >}}4. Poor education:{{< /important >}} People who have to rely on scamming to earn money may have a poor education just like many regular criminals.
<br><br>
However, note that recently, there have been some phishing messages of good grammar. So don’t rely on poor language as a warning signal.
{{< /details >}}

{{< subHeadline >}}Disguised links{{< /subHeadline >}}

Usually, fraudsters disguise their phishing links by choosing a unsuspicious display texts, e.g. “www.wikipedia.org” as the display text for example.com as the actual link: {{< link "https://example.com" >}}www.wikipedia.org{{< /link >}}

{{< details >}}
This is possible because the “Internet language” HTML makes it possible to provide any string of characters with a link, e.g.: {{< link "https://codeberg.org/PhishWarn/PhishWarn">}}Source code{{< /link >}}. In this case {{< important >}}codeberg.org/PhishWarn/PhishWarn{{< /important >}} is the invisible link and “Source code” is the visible display text.
<br><br>
So scammers choose as the visible display text for example “{{< important >}}www.amazon.com/gp/customer-service{{< /important >}}”, but link to a phishing site instead, e.g. example.com (this one is obviously harmless). That looks like this: {{< link "https://example.com">}}www.amazon.com/gp/customer-service{{< /link >}}
<br><br>
But how to recognize this? All major browsers will display the link URL at the screen’s bottom when hovering over the link. Try it out: {{< link "https://twitter.com/snowden">}}example link{{< /link >}}
<br><br>
Only concentrate on the URL preview (not on the possibly faked display text) and determine whether it’s trustworthy or not. If you don’t know, do NOT click on the link.
{{< /details >}}

{{< figure "phishing-explanation/paypal-phishing-explanation.png" "Highly suspicious e-mail address: paypal@notice-access-273.com; vague form of address: “Dear Customer”; severe grammatical mistake: “What the problem’s?”; no sender" >}}
An e-mail seemingly from PayPal that can be identified as highly suspicious on closer inspection.
Good to know: PayPal {{< link "https://www.paypal.com/us/webapps/mpp/security/report-problem" >}}ask{{< /link >}} their users to forward phishing e-mails to {{< link "mailto:spoof@paypal.com">}}spoof@paypal.com{{< /link >}}.
{{< /figure >}}

{{< subHeadline >}}Foreign language{{< /subHeadline >}}

Some scammers simply don’t know or don’t care about their victims’ mother tongues so that for example a German may receive a French phishing mail from his/her German bank. Why would the bank translate their message into a foreign language that their customer probably doesn’t even speak? That wouldn’t make any sense.

{{< subHeadline >}}Request for sensitive user data{{< /subHeadline >}}

The fraudsters’ goal is acquiring the victims’ login credentials and other personal data which is why they openly ask about passwords, PIN numbers or TAN codes. Legit companies, however, would never ever ask about sensitive user data in an e-mail or a chat message because that’s highly insecure. Also, customer supports do NOT need the user’s password as they have full access to the company’s user database.

{{< subHeadline >}}Attachments and calls to action{{< /subHeadline >}}

All phishing messages ask the victim to execute certain instructions, such as klicking on a link or pressing the big shining button. Many scam mails also contain attachments of malicious content (more about that below). Although real emails also instruct the user to do certain things, they are usually far less aggressive or demanding and are not necessarily limited to links and buttons. On the other hand, it is not that common for the real support to send an attachment.

{{< subHeadline >}}Executable file{{< /subHeadline >}}

Some scammers also try to make the victim download a program or application by telling him/her to download and install an attached .exe, .msi, .app or .apk etc. file. Often, such files are also disguised as harmless (e.g. {{< important >}}“dispatchconfirmation.pdf.exe”{{< /important >}}) or hidden from spam filters in a ZIP file.

{{< details >}}
Legit senders would almost certainly not do something like that, but instead
link to a legit website where you can download a program, if needed at all.
<br><br>
{{< important >}}You should never open, download or install anything mentioned in
a suspicious e-mail.{{< /important >}}
<br><br>
{{< important >}}Caution! Many executable files are not recognisable as such!{{< /important >}}
Just like phishing websites are disguised with seemingly legit sub-domains, fraudsters try to disguise the file type of attached executable files by naming them for example {{< important >}}“billing.pdf.exe”{{< /important >}} or {{< important >}}“holidayphoto.jpg.app”{{< /important >}}. In this case the actual file types are {{< important >}}.exe{{< /important >}} and {{< important >}}.app{{< /important >}} – and NOT PDF resp. JPG.
<br><br>
Also, may e-mail programs don’t display the file type by default which is why “billing.pdf” would be displayed instead of “billing.pdf.exe” – which fatally doesn’t look suspicious at all!
<br><br>
Moreover, some scammers hide their executable files inside of .zip archives to prevent e.g. spam filters from detecting the suspicious .exe attachment. If you’re seeing a ZIP file attached you should take extra care!
{{< /details >}}
{{< /article >}}
{{< teaser "preventPhishing" "en" >}}
