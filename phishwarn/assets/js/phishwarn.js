$(document).ready(function(){
  if (getCookie('theme') == 'dark'){
    if (!(window.matchMedia)){
      if (!(window.matchMedia('(prefers-color-scheme: dark)')) && !(window.matchMedia('(prefers-color-scheme: light)').matches)){
        darkmode();
      }

    } else {
      darkmode();
    }
  }

  if('serviceWorker' in navigator){
    navigator.serviceWorker.register('/sw.js')
      .then(reg => console.log('PhishWarn service worker registered'))
      .catch(err => console.log('PhishWarn service worker not registered', err));
  }
});

function darkmode(){
  var head = document.getElementsByTagName('HEAD')[0];
  var link = document.createElement('link');

  link.rel = 'stylesheet';
  link.type = 'text/css';
  link.href = '/css/dark.css';

  head.appendChild(link);
}

$(".header-dark-button").click(function(){
  darkmode();
  setCookie('theme', 'dark', 30);
});

$(".header-light-button").click(function(){
  $("link[href='/css/dark.css']").remove();
  setCookie('theme', '', -1);
  //document.cookie = "theme= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
});

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');

  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
}