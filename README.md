# PhishWarn
Have you ever wanted to see whether your friends would fall for phishing? Just create a phishing mail and link to the PhishWarn website. When the recipient clicks on the link he doesn't go to an actual, harmful phishing site but instead PhishWarn's warning page that tells him that he could have been a victim of internet crime - if the phishing link had actually been harmful.

## Preview

![screenshot](https://codeberg.org/PhishWarn/PhishWarn/raw/branch/master/phishwarn/static/img/phishwarn-preview.png)

## Demo

A demo version is available on PhishWarn's [Codeberg Page](https://phishwarn.codeberg.page).

## Languages

`content` folder:

Currently, PhishWarn supports English and German. If you want to help adding other languages, then fork PhishWarn and translate all `.md` files into the new language by adding the respective language code before the file extension, e.g. `how-to-prevent-being-phished.fr.md` for French.

`i18n` folder:

Copy all the translation strings of `en.yaml` into a new YAML file with the respective language code, e.g. `fr.yaml` for French. Then, translate its content.

## Prepare building

1. Download the latest Hugo Extended `.deb` version from [GitHub](https://github.com/gohugoio/hugo/releases).
2. Install the downloaded `.deb` (use the correct file name!).
3. Install Git and Python.
4. Clone the PhishWarn repository.
5. Create a new output folder named `PhishWarn-pages` and initiate a new Git repository.

```
$ sudo apt install ./hugo_extended_VERSION_Linux-64bit.deb
$ sudo apt install git python3
$ git clone https://codeberg.org/PhishWarn/PhishWarn.git
$ mkdir Grammle-pages && cd Grammle-pages && git init && cd ../Grammle
```

## Build PhishWarn

PhishWarn uses [Hugo](https://gohugo.io/) as its static website generator and [HugoCommit.sh](https://codeberg.org/pixelcode/HugoCommit.sh) for more convenient building, committing and pushing.

```
$ bash HugoCommit.sh
$ bash HugoCommit.sh serve
$ bash HugoCommit.sh commit
```

## Contribution

Feel free to make suggestions etc. by opening a [new issue](https://codeberg.org/pixelcode/PhishWarn/issues/new). I'm happy about every contribution ☺️

## Licence
PhishWarn is licensed under the [For Good Eyes Only Licence v0.2](https://codeberg.org/PhishWarn/PhishWarn/raw/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf), except for the `img` and `fonts` subfolders. Do not re-use PhishWarn's title and logo/icon.

All versions of PhishWarn are licensed under v0.2, which replaces v0.1 under which PhishWarn was previously licensed. v0.1 is therefore void for any versions of PhishWarn.

PhishWarn uses
- [Font Awesome](https://fontawesome.com) ([Font Awesome Free Licence](https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt))
- [Bootstrap Icons](https://icons.getbootstrap.com/) ([MIT Licence](https://github.com/twbs/icons/blob/main/LICENSE.md)).

![](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/banner/Banner%20250.png)